#coding=utf-8
import urllib2
class HtmlDownloader(object):
    def download(self,url):   #返回下载好的url的内容
        if url is None:
            return None
        response =  urllib2.urlopen(url)
        if response.getcode()!= 200:
            return None
        return response.read()