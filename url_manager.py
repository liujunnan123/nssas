#coding=utf-8
class UrlManager(object):
    def __init__(self):
        self.old_urls = set()  # 已爬取过得url列表集合

        self.new_urls = set()  # 未爬取过得url列表集合

        # 向url管理器添加一个新的待爬取的url
    def add_new_urls(self,urls):    #向url管理器添加批量url
        if urls is None or len(urls) ==0:
            return
        for url in urls:
            self.add_new_url(url)
    def add_new_url(self, url):  # 向url管理器添加一个新的url
        if url is None:
            return
            # 若这个url既不在爬取过得url列表集合也不在未爬取的url列表集合
        if url not in self.old_urls and url not in self.new_urls:
            self.new_urls.add(url)  # 将这个url添加到未爬取的url列表集合

    def has_new_url(self):  # 判断url管理器是否有新的url
        return len(self.new_urls) != 0

    def get_new_url(self):  # 从待爬取的url列表中获取一个url
        new_url = self.new_urls.pop()

        self.old_urls.add(new_url)

        return new_url

