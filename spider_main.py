import url_manager, html_downloader, html_parser
class SpiderMain(object):
    def __init__(self):
        self.urls = url_manager.UrlManager()
        self.downloader = html_downloader.HtmlDownloader()
        self.parser = html_parser.HtmlParser()

    #      self.Mysqldb = html_mysqldb.HtmlMysqldb()

    def craw(self, url):
        count = 1
        self.urls.add_new_url(url)
        new_url = self.urls.get_new_url()
        print 'craw %d : %s' % (count, new_url)
        html_cont = self.downloader.download(new_url)
        self.parser.parse(html_cont)

'''
    def craw1(self):
        while self.urls.has_new_url():
            new_url = self.urls.get_new_url()
            print 'craw : %s' % (new_url)
            html_cont = self.downloader.download(new_url)
            new_urls = self.parser.parse1(html_cont)
            self.urls.add_new_urls(new_urls)

    def craw2(self):
        while self.urls.has_new_url():
            new_url = self.urls.get_new_url()
            print 'craw : %s' % (new_url)
            html_cont = self.downloader.download(new_url)
            self.parser.parse2(html_cont)
'''
if __name__ == "__main__":
    url = 'https://nvd.nist.gov/vuln/full-listing/'
    obj_spider = SpiderMain()
    obj_spider.craw(url)